droplab.plugin(function init(DropLab) {
    var self = this;

    // To avoid events just ganging up on the element,
    // lets just all have a fresh start.
    document.querySelectorAll('a').forEach(function(el) {
        var clone = el.cloneNode(true);
        el.parentNode.replaceChild(clone, el);
    });

    var toggleClass = function(elem, className) {
        if (elem.classList && elem.classList.contains(className)) {
            elem.classList.remove(className);

            // FIXME: Just for highlighting right now, not sure
            // where to put styles in this project.
            elem.style.color = 'black';
        }
        else {
            elem.classList.add(className);

            // FIXME: Just for highlighting right now, not sure
            // where to put styles in this project.
            elem.style.color = 'green';
        }
    }

    var addTickMarks = function(elem) {
        var el = elem.querySelector('.tick');

        if (el)
            el.remove();
        else
            elem.innerHTML += ' <span class="tick">✓</span>';
    }

    window.addEventListener('click', function(event) {
        var target = event.target,
            parentTarget = target.parentNode;

        if (target.tagName === 'A' || target.tagName === 'LI') {
            // Let it show.
            document.querySelector('.dropdown-menu').style.display = "block";

            if (!parentTarget.classList.contains('dropdown')) {
                // Adds a class that highlights multiple selections
                toggleClass(target, 'selected');

                // Appends tickMark to the tag
                addTickMarks(target);
            }
        }
        return false;
    });
});
