require('./window')(function(w){
  module.exports = function(deps) {
    deps = deps || {};
    var window = deps.window || w;
    var document = deps.document || window.document;
    var CustomEvent = deps.CustomEvent || require('./custom_event_polyfill');
    var HookButton = deps.HookButton || require('./hook_button');
    var HookInput = deps.HookInput || require('./hook_input');
    var utils = deps.utils || require('./utils');
    var DATA_TRIGGER = require('./constants').DATA_TRIGGER;

    var DropLab = function(hook){
      if (!(this instanceof DropLab)) return new DropLab(hook);
      this.ready = false;
      this.hooks = [];
      this.queuedData = [];
      this.plugins = [];
      if(typeof hook !== 'undefined'){
        this.addHook(hook);
      }
      this.addEvents();
    };

    Object.assign(DropLab.prototype, {
      plugin: function (plugin) {
        this.plugins.push(plugin)
      },

      addData: function () {
        var args = [].slice.apply(arguments);
        if(this.ready) {
          this._addData.apply(this, args);
        } else {
          this.queuedData = this.queuedData || [];
          this.queuedData.push(args);
        }
      },

      _addData: function(trigger, data) {
        this.hooks.forEach(function(hook) {
          if(hook.trigger.dataset.hasOwnProperty('id')) {
            if(hook.trigger.dataset.id === trigger) {
              hook.list.addData(data);
            }
          }
        });
      },

      addEvents: function() {
        var self = this;
        window.addEventListener('click', function(e){
          var thisTag = e.target;
          if(thisTag.tagName === 'LI' || thisTag.tagName === 'A'){
            // climb up the tree to find the UL
            thisTag = utils.closest(thisTag, 'UL');
          }
          if(utils.isDropDownParts(thisTag)){ return }
          if(utils.isDropDownParts(e.target)){ return }
          self.hooks.forEach(function(hook) {
            hook.list.hide();
          });
        });
      },

      addHook: function(hook) {
        if(!(hook instanceof HTMLElement) && typeof hook === 'string'){
          hook = document.querySelector(hook);
        }
        var list = document.querySelector(hook.dataset[utils.toDataCamelCase(DATA_TRIGGER)]);
        if(hook.tagName === 'A' || hook.tagName === 'button') {
          this.hooks.push(new HookButton(hook, list));
        } else if(hook.tagName === 'INPUT') {
          this.hooks.push(new HookInput(hook, list));
        }
      },

      addHooks: function(hooks) {
        hooks.forEach(this.addHook.bind(this));
        return this;
      },

      init: function () {
        this.plugins.forEach(function(plugin) {
          plugin(DropLab);
        })
        var readyEvent = new CustomEvent('ready.dl', {
          detail: {
            dropdown: this,
          },
        });
        window.dispatchEvent(readyEvent);
        this.ready = true;
        this.queuedData.forEach(function (args) {
          this.addData.apply(this, args);
        }.bind(this));
        this.queuedData = [];
        return this;
      },
    });

    return DropLab;
  };
});
