(function(f){if(typeof exports==="object"&&typeof module!=="undefined"){module.exports=f()}else if(typeof define==="function"&&define.amd){define([],f)}else{var g;if(typeof window!=="undefined"){g=window}else if(typeof global!=="undefined"){g=global}else if(typeof self!=="undefined"){g=self}else{g=this}g=(g.droplab||(g.droplab = {}));g=(g.maintain||(g.maintain = {}));g=(g.state||(g.state = {}));g.js = f()}})(function(){var define,module,exports;return (function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
droplab.plugin(function init(DropLab) {
    var self = this;

    // To avoid events just ganging up on the element,
    // lets just all have a fresh start.
    document.querySelectorAll('a').forEach(function(el) {
        var clone = el.cloneNode(true);
        el.parentNode.replaceChild(clone, el);
    });

    var toggleClass = function(elem, className) {
        if (elem.classList && elem.classList.contains(className)) {
            elem.classList.remove(className);

            // FIXME: Just for highlighting right now, not sure
            // where to put styles in this project.
            elem.style.color = 'black';
        }
        else {
            elem.classList.add(className);

            // FIXME: Just for highlighting right now, not sure
            // where to put styles in this project.
            elem.style.color = 'green';
        }
    }

    var addTickMarks = function(elem) {
        var el = elem.querySelector('.tick');

        if (el)
            el.remove();
        else
            elem.innerHTML += ' <span class="tick">✓</span>';
    }

    window.addEventListener('click', function(event) {
        var target = event.target,
            parentTarget = target.parentNode;

        if (target.tagName === 'A' || target.tagName === 'LI') {
            // Let it show.
            document.querySelector('.dropdown-menu').style.display = "block";

            if (!parentTarget.classList.contains('dropdown')) {
                // Adds a class that highlights multiple selections
                toggleClass(target, 'selected');

                // Appends tickMark to the tag
                addTickMarks(target);
            }
        }
        return false;
    });
});

},{}]},{},[1])(1)
});